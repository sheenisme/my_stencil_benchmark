# my_stencil_benchmark

#### 介绍
针对模板(stencil)计算的测试基准程序的源代码。
    
#### 描述
根据<https://sourceforge.net/p/polybench/code/HEAD/tree/>中polybench测试程序和<http://pluto-compiler.sourceforge.net>中pluto编译器的测试examples，另外还参考了国内的CPC竞赛中的3d27pt的测试案例，形成了本仓库的模板(stencil)计算的测试基准程序。虽然并没有囊括全部的stencil计算，但是还算比较完善，另外还有一些小bug，后续将会继续进行修改和完善，敬请期待。

#### 安装教程

1.  git clone <https://gitee.com/sheenisme/my_stencil_benchmark>
2.  编译测试程序
3.  运行测试程序

#### 使用说明

1.  测试程序全部是依据polybench进行修改的，具体的使用步骤可以参考:<https://gitee.com/sheenisme/polybench.git>
2.  建议先多看看polybench的基准程序如何使用,再看本程序，因为本程序只给了源代码，没有编译脚本等信息，编译运行的信息请参考polybench。

#### 参与贡献

1.  未央（gitee: sheenisme）
